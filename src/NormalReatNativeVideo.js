import React, { Component } from 'react';
import { View, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import Video from 'react-native-video';

export default class NormalReatNativeVideo extends Component {
    constructor(props){
        super(props);
        this.state = {
            repeat: false,
            rate: 1,
            volume: 0,
            muted: false,
            resizeMode: 'contain',
            duration: 0.0,
            currentTime: 0.0,
            paused : false,
            pausedText: 'Play',
            rateText: '1.0',
            hideControls: true

        }
        this.video = Video;
    }

    onLoad=(data)=>{
        this.setState({ duration: data.duration })
    }

    onProgress=(data)=>{
        this.setState({ currentTime: data.currentTime })
    }

    onEnd=(data)=>{
        this.setState({ pausedText:'Play', paused:true })
        this.video.seek(0);
    }

    render() {
        const { repeat,rate,volume,muted,resizeMode,duration,currentTime,paused,pausedText,rateText,hideControls } = this.state;
        return (
            <View style={styles.backgroundVideo}>
                <TouchableWithoutFeedback
                    onPress = {() => this.setState({ paused: !this.state.paused })}
                >
                    <Video
                        source={require('./video/angular.mp4')}
                        ref={(ref) => { this.video = ref }}
                        style={{ width:500, height: 200,backgroundColor:'yellow' }}
                        repeat={repeat}
                        rate={rate}
                        rateText={rateText}
                        volume={volume}
                        muted={muted}
                        resizeMode={resizeMode}
                        duration={duration}
                        currentTime={currentTime}
                        paused={paused}
                        pausedText={pausedText}
                        hideControls={hideControls}
                        ignoreSilentSwitch={"obey"}

                        onLoad={this.onLoad}
                        onProgress={this.onProgress}
                        onEnd={this.onEnd}


                    />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

// Later on in your styles..
var styles = StyleSheet.create({
    backgroundVideo: {
        position: 'absolute',
        backgroundColor: 'black',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});