import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import VideoPlayer from 'react-native-video-controls';

export default class ReactNativeVideoControl extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <VideoPlayer
                source={require('./video/angular.mp4')}
                navigator={this.props.navigator}
            />
        )
    }
}

// Later on in your styles..
var styles = StyleSheet.create({
    backgroundVideo: {
        position: 'absolute',
        backgroundColor: 'black',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});